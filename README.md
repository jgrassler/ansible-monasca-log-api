# monasca-log-api
Installs the [monasca-log-api](https://github.com/openstack/monasca-log-api)
part of the [Monasca](https://wiki.openstack.org/wiki/Monasca) project.

Role installs Python implementation

    Java support has been dropped. For now only supported
    implementation for monasca-log-api is Python.

## Runtime control
* log_api_run_mode:
 * Deploy - Full run including: [Install, Configure, Start, Status]
 * Install - Just installation. Configuration, starting is skipped
 * Configure - Just configuration. Installation, starting is skipped
 * Start - Starts the service, includes checking status
 * Stop - Stops the service
 * Status - Allows to verify the status

## Common
* api_region - determines region for the API
* log_topic - target topic in Kafka
* log_api_log_level - log level that you want to install application with
* log_api_handler_log_level - log level for handlers that you want to install application with
* log_api_version - version of monasca-log-api
* log_api_virtualenv_dir - virtualenv location for monasca-log-api python implementation
* log_api_max_log_size - message payload size that can be send to monasca-log-api
* log_api_max_message_size - message max size that can be send to kafka
* log_api_file_size - log file size in MB to rotate
* default_authorized_roles - Comma seperated list of Monasca authorized roles.
* agent_authorized_roles - Comma seperated list of Monasca agent authorized roles.
* log_api_install_user_group - should install system user&group

## WSGI support
* log_api_use_wsgi (false) - set to ```true``` if you wish **monasca-log-api** to be deployed
over with WSGI

Following paths ```log_api_wsgi_conf_dir``` and ```log_api_wsgi_log_dir``` are set based on detected
```ansible_os_family``` and may vary between distributions.

```log_api_wsgi_config``` is set based on ```log_api_wsgi_conf_dir```
```log_api_wsgi_script``` points to **WSGIScriptAlias**

## Optional
* memcached_nodes - List of memcached servers

### SSL
* log_api_certfile - In terms of using ssl use this configuration to set the path to ssl certfile
* log_api_keyfile - In terms of using ssl use this configuration to set the path to ssl keyfile

<!-- TODO(trebskit) fill in gaps in variables -->

## License

Apache License, Version 2.0

## Author Information

Kamil Choroba
